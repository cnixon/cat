use std::io::{self, Write};
use std::env::{self};
use std::fs::File;
use std::process;


fn main() {
    let mut args = env::args();
    match cat(&mut args){
        Ok(_) => {process::exit(0);}
        Err(e) => {
            match writeln!(&mut io::stderr(), "{}" , e) {
                Ok(_) => {},
                Err(err) => panic!("Unable to write to stderr: {}", err),
            }
            process::exit(1);
        }
    }
}


fn cat(args : &mut Iterator<Item=String>) -> Result<(), String> {

    // Get a handle to stdin and stdout
    let mut stdout = io::stdout();
    let mut stdin = io::stdin();

    // Skip the first element of args as it's the name of the executable
    let args = args.skip(1);

    // Make a peekable iterator of args so we can check for flags
    let mut args = args.peekable();

    // Handle args (we only support -u)
    if args.peek().map_or(false, | arg | {arg == "-u"}){
        // and we do don't buffer anyway, so just drop it
        args.next();
    };

    // handle arguments
    for arg in args{
        match arg.as_str(){
            // If we encounter "-" we should read from stdin til eof
            "-" => {
                if let Err(_) = io::copy(&mut stdin, &mut stdout) {
                    return Err("cat: Couldn't copy from stdin to stdout".to_string())
                }
            }
            // The only other thing we accept are file names
            _ => {
                if let Ok(mut fh) = File::open(&arg) {
                    if let Err(_) = io::copy(&mut fh, &mut stdout){
                        return Err(format!("Couldn't copy from {:?} to stdout", &fh))
                    }
                } else {
                    return Err(format!("cat: {}: No such file or directory", &arg))
                }
            }
        }
    }
    Ok(())
}
